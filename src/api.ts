import axios from 'axios';

const Api = (): any => {
  const defaultOptions = {
    baseURL: process.env.REACT_APP_BASE_URL,
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Create instance
  const instance = axios.create(defaultOptions);

  // Set the AUTH token for any request
  instance.interceptors.request.use((configArg: any) => {
    const config = configArg;
    const token = localStorage.getItem('token');
    config.headers.Authorization = token || '';
    return config;
  });

  return instance;
};

export default Api;
