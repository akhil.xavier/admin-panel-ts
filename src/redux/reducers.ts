/* eslint-disable  */
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import history from 'utils/history';

import Auth from './App/reducer';

const rootReducer = combineReducers({
  router: connectRouter(history),
  Auth,
});
export default rootReducer;
