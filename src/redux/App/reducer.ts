import produce from 'immer';
import { types, AppTypes } from './types';

const { LOGIN, LOGOUT } = types;

const initialState = {
  authenticated: false,
};

const reducer = (state = initialState, action: AppTypes) =>
  produce(state, (draft) => {
    const draftState = draft;
    switch (action.type) {
      case LOGIN:
        draftState.authenticated = true;
        return draftState;
      case LOGOUT:
        draftState.authenticated = false;
        return draftState;
      default:
        return state;
    }
  });

export default reducer;
