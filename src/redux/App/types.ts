export const types = {
  LOGIN: 'LOGIN',
  LOGOUT: 'LOGOUT',
};

interface LoginAction {
  type: typeof types.LOGIN;
}
interface LogoutAction {
  type: typeof types.LOGOUT;
}

export type AppTypes = LoginAction | LogoutAction;
