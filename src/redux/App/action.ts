import { types, AppTypes } from './types';

export const loginAction = (): AppTypes => ({
  type: types.LOGIN,
});

export const logoutAction = (): AppTypes => ({
  type: types.LOGOUT,
});
