import React, { lazy } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';

const App: React.FC = () => {
  return (
    <Switch>
      <Route
        exact
        path="/login"
        component={lazy(() => import('pages/Login'))}
      />
      <Route path="/" component={lazy(() => import('components/App'))} />
    </Switch>
  );
};

export default withRouter(App);
