import { createMuiTheme } from '@material-ui/core/styles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';

const theme: Theme = createMuiTheme({});

export default theme;
