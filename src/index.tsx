import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { ConnectedRouter } from 'connected-react-router';
import { ThemeProvider as MuThemeProvider } from '@material-ui/core/styles';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import configureStore from 'redux/configureStore';
import history from 'utils/history';
import App from 'publicRoutes';
import * as serviceWorker from 'serviceWorker';
import theme from 'theme';

import 'css/index.css';

const { store, persistor } = configureStore(history);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <MuThemeProvider theme={theme}>
          <Suspense fallback={<div>...Loading</div>}>
            <ConnectedRouter history={history}>
              <App />
            </ConnectedRouter>
          </Suspense>
        </MuThemeProvider>
      </PersistGate>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
