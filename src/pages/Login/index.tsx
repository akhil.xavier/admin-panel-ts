import React from 'react';
import { Form, Formik } from 'formik';
import { withRouter } from 'react-router-dom';
import * as Yup from 'yup';
import { useDispatch } from 'react-redux';

import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import { Button, TextField } from '@material-ui/core';

import logo from 'assets/images/logo.svg';
import { loginAction } from 'redux/App/action';
import useStyles from './style';
import 'css/App.css';

const DefaultUIValidation = Yup.object().shape({
  email: Yup.string().required('Required'),
  password: Yup.string().required('Required'),
});

interface Props {
  history: any;
}

const Login: React.FC<Props> = ({ history }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  return (
    <>
      <CssBaseline />
      <Container className={classes.container} fixed>
        <img src={logo} className="App-logo" alt="logo" />
        <Formik
          initialValues={{ email: '', password: '' }}
          validationSchema={DefaultUIValidation}
          onSubmit={(values) => {
            if (values.email === 'root' && values.password === 'root') {
              dispatch(loginAction());
              history.push('/');
            }
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
          }) => (
            <Form className={classes.formContainer} onSubmit={handleSubmit}>
              <TextField
                className={classes.textField}
                name="email"
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
                helperText={errors.email && touched.email && errors.email}
                error={!!(errors.email && touched.email)}
                label="Email"
                margin="normal"
                variant="outlined"
              />
              <TextField
                className={classes.textField}
                name="password"
                label="Password"
                type="password"
                autoComplete="current-password"
                margin="normal"
                variant="outlined"
                value={values.password}
                onChange={handleChange}
                onBlur={handleBlur}
                error={!!(errors.password && touched.password)}
                helperText={
                  errors.password && touched.password && errors.password
                }
              />
              <Button
                className={classes.button}
                type="submit"
                variant="contained"
              >
                Login
              </Button>
            </Form>
          )}
        </Formik>
      </Container>
    </>
  );
};

export default withRouter(Login);
